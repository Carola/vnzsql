<?php
/* vnzsql/base/class.astractvnz.php
*
* Copyright (C)  2014 by  Misa3l (blacksecured@gmail.com) .
* Este archivo es parte de VnzSQL .
* https://github.com/misa3l .
* Licenciado bajo la AGPL versión 3 o superior .
*/

/**
 * Define la version de vnzsql
 */
define("VNZ_VERSION", 0.1);

/**
 * VNZ Valor predefinido usado en get_todo() get_columnas() execute() etc.
 */
define("VNZ_VALOR_PREDEFINIDO", -1);

/**
 * Respuesta de tipo ASSOC
 */
define("VNZ_RES_ASSOC", 1);

/**
 * Respuesta de tipo NUM
 */
define("VNZ_RES_NUM", 2);

/**
 * Respuesta de tipo VNZ_RES_ASSOC + VNZ_RES_NUM ( AMBAS )
 */
define("VNZ_RES_AMBAS", 3);

/**
 * VNZ Valor predefinido 0 aun sin uso
 */
define("VALOR_PREDEFINIDO", 0);

/**
 * Array Asociativo aun sin uso
 */
define("ARRAY_ASOCIATIVO", 1);

/**
 * Array Numérico aun sin uso
 */
define("ARRAY_NUMERICO", 2);

/**
 * Ambas aun sin uso
 */
define("AMBAS", 3);

/**
 * Expresión para extraer el nombre de una tabla
 */
define("REGTABLE", '/^SELECT\s+(?:ALL\s+|DISTINCT\s+)?(?:.*?)\s+FROM\s+(.*)$/i');

/**
 * Expresión para verificar si la consulta es valida
 */
define("REGSQL", '%^(select|update|insert|delete|create|show|drop|set|)%is');

/**
 * Componente de Abstracción (VNZSQL database abstraction library)
 *
 * @package ClassAstractDB
 * @author Misa3l (blacksecured@gmail.com)
 * @copyright Misa3l (blacksecured@gmail.com)
 * @version 1.0
 * @access public
 */

abstract class ClassAstractDB {

    /**
     * Host de la Conexión
     * @var string
     * @access public
     */
    public $host = '';

    /**
     * Base de datos de la Conexión
     * @var string
     * @access public
     */
    public $basedatos = '';

    /**
     * Usuario de la Conexión
     * @var string
     * @access public
     */
    public $usuario = '';

    /**
     * Clave de la Conexión
     * @var string
     * @access public
     */
    public $clave = '';

    /**
     * Puerto de la Conexión
     * @var integer
     * @access public
     */
    public $puerto = 0;
    
    /**
     * Link de Conexión
     * @var object|boolean
     * @access public
     */
    public $db = NULL;

    /**
     * Resultado de una consulta
     * @var boolean
     * @access public
     */
    public $resultado = NULL;

    /**
     * Tipo de respuesta (por defecto VNZ_RES_ASSOC)
     * @var string
     * @access public
     */
    public $prefResTipo = '';

    /**
     * Ultimo query
     * @var string
     * @access public
     */
    public $lastQuery = '';

    /**
     * Flag de persistencia para la conexión
     * @var boolean
     * @access public
     */
    public $persistente = FALSE;
    
    /**
     * Tipo de error
     * @var string
     * @access public
     */
    public $error = '';

    /**
     * Numero de rows de una consulta
     * @var integer
     * @access public
     */
    public $num_rows = 0;

    /**
     * Array multiuso
     * @var array
     * @access public
     */
    public $array = array();

    /**
     * Posición de get_variable
     * @var integer
     * @access public 
     */
    public $posicion = 0;
    
    /**
     * Objeto multiuso
     * @var object
     * @access public
     */
    public $obj = NULL;

    /**
     * Charset de la conexión
     * @var string
     * @access public
     */
    public $charset = '';

    /**
     * Tipo de conexión
     * @var array
     * @access private
     */
    public $tipos_basedatos = array();
    
    /**
     * Método __construct Abstracto
     * @param string $dbtipo
     * @param string $prefResTipo
     */
    private function __construct($dbtipo, $prefResTipo = VNZ_RES_ASSOC) {
        
    }

    /**
     * Método conectar Abstracto
     * @param string $host
     * @param string $basedatos
     * @param string $usuario
     * @param string $clave
     * @param integer $puerto
     * @param boolean $persistente
     */
    public function conectar($host, $basedatos, $usuario, $clave, $puerto, $persistente = FALSE) {
        
    }

    /**
     * Método set_charset Abstracto
     * @param string $charset
     */
    public function set_charset($charset) {
        
    }

    /**
     * Método query Abstracto
     * @param string $query
     */
    public function query($query) {
        
    }

    /**
     * Método get_ultimoquery Abstracto
     */
    public function get_ultimoquery() {
        
    }

    /**
     * Método get_siguiente Abstracto
     * @param string $resultadoTipo
     */
    public function get_siguiente($resultadoTipo = VNZ_VALOR_PREDEFINIDO) {
        
    }

    /**
     * Método get_todo Abstracto
     * @param string $resultadoTipo
     */
    public function get_todo($resultadoTipo = VNZ_VALOR_PREDEFINIDO) {
        
    }
    
    /**
     * Método get_variable Abstracto
     * @param string $query
     * @param integer $posicion
     */
    public function get_variable($query,$posicion = 0){
        
    }

    /**
     * Método get_database Abstracto
     */
    public function get_database(){
        
    }
    
    /**
     * Método get_nombrecolumnas Abstracto
     */
    public function get_nombrecolumnas() {
        
    }

    /**
     * Método get_columnas Abstracto
     * @param string $resultadoTipo
     */
    public function get_columnas($resultadoTipo = VNZ_VALOR_PREDEFINIDO) {
        
    }

    /**
     * Método execute Abstracto
     * @param string $query
     * @param string $resultadoTipo
     */
    public function execute($query, $resultadoTipo = VNZ_VALOR_PREDEFINIDO) {
        
    }

    /**
     * Método liberar Abstracto
     */
    public function liberar() {
        
    }

    /**
     * Método numero_rows Abstracto
     */
    public function numero_rows() {
        
    }

    /**
     * Método rows_afectados Abstracto
     */
    public function rows_afectados() {
        
    }

    /**
     * Método fobject Abstracto
     */
    public function fobject() {
        
    }

    /**
     * Método get_identificador Abstracto
     */
    public function get_identificador() {
        
    }

    /**
     * Método get_dbtipo Abstracto
     */
    public function get_dbtipo() {
        
    }
    
    

    /**
     * Método get_version Abstracto
     */
    public function get_version() {
        
    }

    /**
     * Método escape Abstracto
     * @param string|array $str
     */
    public function escape($str) {
        
    }

    /**
     * Método get_tablas Abstracto
     */
    public function get_tablas() {
        
    }

    /**
     * Método get_uno Abstracto
     * @param string $query
     */
    public function get_uno($query) {
        
    }

    /**
     * Método array2obj Abstracto
     * @param array $array
     */
    public function array2obj($array) {
        
    }

    /**
     * Método obj2array Abstracto
     * @param object $obj
     */
    public function obj2array($obj) {
        
    }
    
    /**
     * Método desconectar Abstracto
     */
    public function desconectar() {
        
    }

}

?>