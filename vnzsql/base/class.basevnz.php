<?php
/* vnzsql/base/class.basevnz.php
*
* Copyright (C)  2014 by  Misa3l (blacksecured@gmail.com) .
* Este archivo es parte de VnzSQL .
* https://github.com/misa3l .
* Licenciado bajo la AGPL versión 3 o superior .
*/

require_once('class.astractvnz.php');

/**
 * Componente Base (VNZSQL database abstraction library)
 *
 * @package ClassBaseVNZ
 * @author Misa3l (blacksecured@gmail.com)
 * @copyright Misa3l (blacksecured@gmail.com)
 * @version 1.0
 * @access public
 */

class ClassBaseVNZ extends ClassAstractDB {

    /**
     * Tipo de base de datos
     * @var string
     * @access private
     */
    public $dbtipo;

    /**
     * Método Constructor Base
     * @param string $dbtipo
     * @param string $prefResTipo
     */
    private function __construct($dbtipo, $prefResTipo = VNZ_RES_ASSOC) {
        $this->prefResTipo = $prefResTipo;
        $this->dbtipo = $dbtipo;
        $this->_id = "Clase base abstracta";
    }

    /**
     * Método conectar Base inicializa las propiedades del Método conectar
     * @param string $host
     * @param string $basedatos
     * @param string $usuario
     * @param string $clave
     * @param integer $puerto
     * @param boolean $persistente
     * @return boolean
     */
    public function conectar($host, $basedatos, $usuario, $clave, $puerto, $persistente = FALSE) {
        $this->host = $host;
        $this->database = $basedatos;
        $this->usuario = $usuario;
        $this->clave = $clave;
        $this->puerto = $puerto;
        $this->persistente = $persistente;
        return TRUE;
    }

    /**
     * Método set_dbtipo Base Envió el tipo de Base de datos
     * @param string $dbtipo
     */
    public function set_dbtipo($dbtipo) {
        $this->dbtipo = $dbtipo;
    }
    
    /**
     * Método query Base guarda el ultimo query y liberar memoria
     * @param string $query
     */
    public function query($query) {
        $this->lastQuery = $query;
        $this->error = '';
        $this->liberar();
    }

    /**
     * Método get_ultimoquery Base retorna el ultimo query ejecutado
     * @return string
     */
    public function get_ultimoquery() {
        parent::get_ultimoquery();
        return $this->lastQuery;
    }

    /**
     * Método get_todo Base retorna un array completo de una consulta
     * @param string $resultadoTipo
     * @return array
     */
    public function get_todo($resultadoTipo = VNZ_VALOR_PREDEFINIDO) {
        if ($resultadoTipo == VNZ_VALOR_PREDEFINIDO) {
            $resultadoTipo = $this->prefResTipo;
        }
        $res = array();
        while ($col = $this->get_siguiente($resultadoTipo)) {
            array_push($res, $col);
        }
        return $res;
    }

    /**
     * Método get_variable retornara un string único
     * @param string $query
     * @param integer $posicion
     */
    public function get_variable($query,$posicion = 0){
        if($this->query($query)){
            $valores = array_values($this->get_siguiente());
        }
        return (isset($valores[$posicion]) && $valores[$posicion] !== '') ? $valores[$posicion]:null;
    }
    
    /**
     * Método execute Base ejecuta una query y retorna un array completo
     * @param string $query
     * @param string $resultadoTipo
     * @return boolean|array
     */
    public function execute($query, $resultadoTipo = VNZ_VALOR_PREDEFINIDO) {
        if ($this->query($query)) {
            return $this->get_todo($resultadoTipo);
        } else {
            return FALSE;
        }
    }

    /**
     * Método get_columnas Base retorna un array con todos sus columnas y valores
     * @param string $resultadoTipo
     * @return array
     */
    public function get_columnas($resultadoTipo = VNZ_VALOR_PREDEFINIDO) {
        if ($resultadoTipo == VNZ_VALOR_PREDEFINIDO) {
            $resultadoTipo = $this->prefResTipo;
        }
        $array = $this->get_todo($resultadoTipo);
        $res = array();

        for ($i = 0; $i < sizeof($array); $i++) {
            $value = $this->_fixtipo($array[$i], $resultadoTipo);
            if ($value != false) {
                $res[$i] = $value;
            }
        }
        return $res;
    }

    /**
     * Método liberar Base retorna true
     * @return boolean
     */
    public function liberar() {
        return TRUE;
    }

    /**
     * Método get_dbtipo Base retorna el tipo de base de datos
     * @return string
     */
    public function get_dbtipo() {
        return $this->dbtipo;
    }

    /**
     * Método get_version Base retorna la Versión de VNZSQL
     * @return string
     */
    public function get_version() {
        return "VNZSQL: " . VNZ_VERSION;
    }

    /**
     * Método get_identificador Base retorna el identificador usado
     * @return string
     */
    public function get_identificador() {
        return $this->_id;
    }

    /**
     * Método get_database Base retorna el nombre de las base de datos
     * @return boolean|array
     */
    public function get_database() {
        parent::get_database();
        $res = array();
        switch ($this->get_dbtipo()) {
            // mysql
            case 'mysql':
            case 'mysqli':
            case 'mysqll':
            case 'mysqlt':
                $this->query('SHOW DATABASES');
                while ($datab = $this->get_siguiente($this->prefResTipo)) {
                    array_push($res, $datab);
                }
                return $res;
            // postgresql
            case 'pgsql':
            case 'postgres':
            case 'postgres64':
            case 'postgres7':
            case 'postgres9':
                $this->query('select datname as name from pg_database');
                while ($datab = $this->get_siguiente($this->prefResTipo)) {
                    array_push($res, $datab);
                }
                return $res;
            case 'sqlite3';
                $this->query('SELECT name FROM sys.Databases');
                while ($datab = $this->get_siguiente($this->prefResTipo)) {
                    array_push($res, $datab);
                }
                return $res;
                break;

            default:
                $this->error = 'Comando Desconocido!!';
                return FALSE;
        }
        return $this->get_tablas();
    }
    
    /**
     * Método get_tablas Base retorna las tablas de una base de datos
     * @return boolean|array
     */
    public function get_tablas() {
        $tabl = array();
        switch ($this->get_dbtipo()) {
            // mysql
            case 'mysql':
            case 'mysqli':
            case 'mysqll':
            case 'mysqlt':
                $this->query('show tables');
                while ($row = $this->get_siguiente($this->prefResTipo)){
                    array_push($tabl, $row);
                }
                return $tabl;
            // postgresql
            case 'pgsql':
            case 'postgres':
            case 'postgres64':
            case 'postgres7':
            case 'postgres9':
                $this->query('select tablename from pg_tables where tableowner = current_user');
            //$this->query("SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'");
                while ($row = $this->get_siguiente($this->prefResTipo)){
                    array_push($tabl, $row);
                }
                return $tabl;
            //sqlite3
            case 'sqlite3';
                $this->query("SELECT name FROM sqlite_master WHERE type = 'table' ");
                while ($row = $this->get_siguiente($this->prefResTipo)){
                    array_push($tabl, $row);
                }
                return $tabl;
                break;

            default:
                $this->error = 'Comando Desconocido!!';
                return FALSE;
        }
        return $this->get_columnas();
    }

    /**
     * Método escape Base retorna una cadena o array escapada
     * @param string|array $str
     * @return string
     */
    public function escape($str) {
        if (is_array($str)) {
            $res = array();
            foreach ($str as $key => $valor) {
                $res[$key] = $this->escape($valor);
            }
            return $res;
        } else {
            switch ($this->dbtipo) {
                // mysql
                case 'mysql':
                case 'mysqll':
                case 'mysqlt':
                    return mysql_real_escape_string(stripslashes($str));
                case 'mysqli':
                    return mysqli_real_escape_string($this->db, stripslashes($str));
                // postgresql
                case 'pgsql':
                case 'postgres':
                case 'postgres64':
                case 'postgres7':
                case 'postgres8':
                case 'postgres9':
                    return pg_escape_string(stripslashes($str));
                case 'sqlite3':
                    return trim(htmlentities(SQLite3::escapeString($str)));
                    break;
            }
            return '';
        }
    }
    
    /**
     * Método _AntesDesconectar Base inicializa todo por defecto
     * @return boolean
     */
    public function _AntesDesconectar() {
        $this->db = null;
        $this->host = '';
        $this->database = '';
        $this->usuario = '';
        $this->clave = '';
        $this->puerto = 0;
        return true;
    }
    
    /**
     * Método array2obj Base transforma un array en un objeto
     * @param array $array
     * @return boolean|object
     */
    public function array2obj($array) {
        parent::array2obj($array);
        if (is_array($array)) {
            $obj = (object)$array;
            return $obj;
        } else {
            $this->error = "array2obj array FALSE";
            return false;
        }
    }

    /**
     * Método obj2array Base transforma un objeto en un array
     * @param array $array
     * @return boolean|array
     */
    public function obj2array($obj) {
        parent::obj2array($obj);
        if (is_object($obj)) {
            $array = array();
            $array = (array)$obj;
            return $array;
        } else {
            $this->error = "obj2array array FALSE";
            return false;
        }
    }
    
    /**
     * Método _fixtipo Base
     * @param string $res
     * @param string $resultadoTipo
     * @return string
     */
    private function _fixtipo($res, $resultadoTipo = VNZ_RES_NUM) {
        $temp = array_values($res);
        $cont = 1;
        if ($resultadoTipo == VNZ_RES_AMBAS) {
            $cont += 1;
        }
        if (sizeof($temp) == $cont) {
            return $temp[0];
        } else {
            return $res;
        }
    }

}

?>