<?php
/* vnzsql/class.postgresdb.php
*
* Copyright (C)  2014 by  Misa3l (blacksecured@gmail.com) .
* Este archivo es parte de VnzSQL .
* https://github.com/misa3l .
* Licenciado bajo la AGPL versión 3 o superior .
*/

/**
 * Componente POSTGRES (VNZSQL database abstraction library)
 *
 * @package vnzsql_postgres
 * @author Misa3l (blacksecured@gmail.com)
 * @copyright Misa3l (blacksecured@gmail.com)
 * @version 1.0
 * @access public
 */

class vnzsql_postgres extends ClassBaseVNZ {

    /**
     * Instancia para el patrón de diseño singleton (instancia única)
     * @var object instancia
     * @access private
     */
    private static $instancia;

    /**
     * Método Constructor
     * @param string $dbtipo
     * @param string $prefResTipo
     */
    private function __construct($dbtipo = "postgres", $prefResTipo = VNZ_RES_ASSOC) {
        $this->prefResTipo = $prefResTipo;
        $this->dbtipo = $dbtipo;
        $this->_id = "POSTGRES";
    }

    /**
     * Realiza la instancia
     * @return object
     */
    public static function singleton() {
        if (!isset(self::$instancia)) {
            $clase = __CLASS__;
            self::$instancia = new $clase;
        }
        return self::$instancia;
    }

    /**
     * Método Clone
     */
    public function __clone() {
        trigger_error("La clonación de este objeto no está permitida ", E_USER_ERROR);
    }

    /**
     * Método wakeup
     */
    public function __wakeup() {
        trigger_error("No puede deserializar una instancia de " . get_class($this) . " Class. ", E_USER_ERROR);
    }

    /**
     * Método Destructor
     */
    public function __destruct() {
        $this->desconectar();
    }

    /**
     * Envía el tipo de respuesta
     * @param string $prefResTipo
     */
    public function set_tiporespuesta($prefResTipo = VNZ_RES_ASSOC) {
        $this->prefResTipo = $prefResTipo;
    }

    /**
     * Conectar retorna true si conecto con éxito, o falso si no hay conexión
     * @param string $host
     * @param string $basedatos
     * @param string $usuario
     * @param string $clave
     * @param integer $puerto
     * @param boolean $persistente
     * @return boolean
     */
    public function conectar($host, $basedatos, $usuario, $clave, $puerto = 5432, $persistente = FALSE) {
        parent::conectar($host, $basedatos, $usuario, $clave, $puerto, $persistente);
        $connection_string = "host=$host port=$puerto dbname=$basedatos user=$usuario password=$clave";
        $this->db = @pg_connect($connection_string);
        if (is_resource($this->db)) {
            return true;
        } else {
            $this->error = pg_last_error($this->db);
            return false;
        }
    }

    /**
     * Envía el charset a la conexión
     * @param string $charset
     * @return boolean
     */
    public function set_charset($charset) {
        parent::set_charset($charset);
        if ($this->db != null) {
            $rs = @pg_set_client_encoding($this->db, $charset);
            if ($rs == 0) {
                return true;
            } else {
                $this->error = "set_charset FALSE";
                return false;
            }
        } else {
            $this->error = "set_charset This DB FALSE";
            return false;
        }
    }

    /**
     * Ejecuta las Consultas a la base de datos
     * @param string $query
     * @return boolean
     */
    public function query($query) {
        parent::query($query);
        $this->_contador = 0;
        if (is_resource($this->db)) {
            $this->resultado = @pg_query($this->db, $query);
            if ($this->resultado != false) {
                return true;
            } else {
                $this->error = @pg_last_error($this->db);
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Retorna el nombre de las columnas
     * @return boolean|array
     */
    public function get_nombrecolumnas() {
        parent::get_nombrecolumnas();
        $array = array();
        if ($this->resultado != false) {
            $numf = @pg_num_fields($this->resultado);
            for ($i = 0; $i < $numf; $i++) {
                $valores = @pg_field_name($this->resultado, $i);
                array_push($array, $valores);
            }
            return $array;
        } else {
            $this->error = "get_nombrecolumnas FALSE";
            return false;
        }
    }

    /**
     * Retorna el resultado de una consulta (Dependiendo mucho del tipo de resultado enviado Ver línea 70)
     * @param string $resultadoTipo
     * @return boolean|array
     */
    public function get_siguiente($resultadoTipo = VNZ_VALOR_PREDEFINIDO) {
        if ($resultadoTipo == VNZ_VALOR_PREDEFINIDO) {
            $resultadoTipo = $this->prefResTipo;
        }
        $res = false;
        if ($this->resultado != null) {
            switch ($resultadoTipo) {
                case VNZ_RES_ASSOC:
                    $res = @pg_fetch_array($this->resultado, $this->_contador, PGSQL_ASSOC);
                    break;
                case VNZ_RES_NUM:
                    $res = @pg_fetch_array($this->resultado, $this->_contador, PGSQL_NUM);
                    break;
                case VNZ_RES_AMBAS:
                    $res = @pg_fetch_array($this->resultado, $this->_contador, PGSQL_BOTH);
                    break;
                default:
                    $this->error = "Tipo de resultado incorrecto!";
            }
            $this->_contador += 1;
        }
        return $res;
    }

    /**
     * Liberar el resultado de una consulta
     * @return boolean
     */
    public function liberar() {
        if ($this->resultado != null) {
            return @pg_free_result($this->resultado);
        } else {
            $this->error = "liberar FALSE";
            return false;
        }
    }

    /**
     * Retorna el número de rows en una consulta
     * @return boolean|integer
     */
    public function numero_rows() {
        if ($this->resultado != null) {
            return @pg_num_rows($this->resultado);
        } else {
            $this->error = "numero_rows FALSE";
            return false;
        }
    }

    /**
     * Realiza una consulta a la base de datos retorna un objeto
     * @return boolean|object
     */
    public function fobject() {
        parent::fobject();
        if ($this->resultado != null) {
            return @pg_fetch_object($this->resultado);
        } else {
            $this->error = "fobject FALSE";
            return false;
        }
    }

    /**
     * Realiza una consulta a la base de datos retorna un único objeto
     * @param string $query
     * @return boolean|object
     */
    public function get_uno($query) {
        parent::get_uno($query);
        $this->query($query);
        if ($this->resultado != NULL) {
            $row = @pg_fetch_object($this->resultado);
            return $row;
        } else {
            $this->error = "get_uno query error";
            return FALSE;
        }
    }

    /**
     * Retorna el número de rows afectados por una consulta
     * @return boolean|integer
     */
    public function rows_afectados() {
        if ($this->db != null) {
            return @pg_affected_rows($this->resultado);
        } else {
            $this->error = "rows_afectados FALSE";
            return false;
        }
    }

    /**
     * Realiza la Desconexión
     * @return boolean
     */
    public function desconectar() {
        parent::desconectar();
        if ($this->db != null) {
            @pg_close($this->db);
            $this->_AntesDesconectar();
            return true;
        } else {
            $this->error = "No Conectado a ninguna Base de Datos!";
            return false;
        }
    }

}

?>