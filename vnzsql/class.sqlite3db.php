<?php
/* vnzsql/class.sqlite3db.php
*
* Copyright (C)  2014 by  Misa3l (blacksecured@gmail.com) .
* Este archivo es parte de VnzSQL .
* https://github.com/misa3l .
* Licenciado bajo la AGPL versión 3 o superior .
*/

/**
 * Componente SQLITE3 (VNZSQL database abstraction library)
 *
 * @package vnzsql_sqlite3
 * @author Misa3l (blacksecured@gmail.com)
 * @copyright Misa3l (blacksecured@gmail.com)
 * @version 1.0
 * @access public
 */

class vnzsql_sqlite3 extends ClassBaseVNZ {

    /**
     * Instancia para el patrón de diseño singleton (instancia única)
     * @var object instancia
     * @access private
     */
    private static $instancia;

    /**
     * Método Constructor
     * @param string $dbtipo
     * @param string $prefResTipo
     */
    private function __construct($dbtipo = "sqlite3", $prefResTipo = VNZ_RES_ASSOC) {
        $this->prefResTipo = $prefResTipo;
        $this->dbtipo = $dbtipo;
        $this->_id = "SQLITE3";
    }

    /**
     * Realiza la instancia
     * @return object
     */
    public static function singleton() {
        if (!isset(self::$instancia)) {
            $clase = __CLASS__;
            self::$instancia = new $clase;
        }
        return self::$instancia;
    }

    /**
     * Método Clone
     */
    public function __clone() {
        trigger_error("La clonación de este objeto no está permitida ", E_USER_ERROR);
    }

    /**
     * Método wakeup
     */
    public function __wakeup() {
        trigger_error("No puede deserializar una instancia de " . get_class($this) . " Class. ", E_USER_ERROR);
    }

    /**
     * Método Destructor
     */
    public function __destruct() {
        $this->desconectar();
    }

    /**
     * Envía el tipo de respuesta
     * @param string $prefResTipo
     */
    public function set_tiporespuesta($prefResTipo = VNZ_RES_ASSOC) {
        $this->prefResTipo = $prefResTipo;
    }

    /**
     * Conectar retorna true si conecto con éxito, o falso si no hay conexión
     * @param string $host
     * @param string $basedatos
     * @param string $usuario
     * @param string $clave
     * @param integer $puerto
     * @param boolean $persistente
     * @return boolean
     */
    public function conectar($host, $basedatos, $usuario, $clave, $puerto = 0, $persistente = FALSE) {
        parent::conectar($host, $basedatos, $usuario, $clave, $puerto, $persistente);
        $this->db = new SQLite3($basedatos);
        if (is_object($this->db)) {
            return TRUE;
        } else {
            $this->error = "Error al conectar a Base de datos";
            return FALSE;
        }
    }

    /**
     * Ejecuta las Consultas a la base de datos
     * @param string $query
     * @return boolean
     */
    public function query($query) {
        parent::query($query);
        if ($this->db != null) {
            $this->resultado = $this->db->query($query);
            if ($this->resultado) {
                return TRUE;
            } else {
                $this->resultado = null;
                $this->error = "query resultado FALSE";
                return FALSE;
            }
        }
    }

    /**
     * Retorna el nombre de las columnas
     * @return boolean|array
     */
    public function get_nombrecolumnas() {
        if ($this->resultado != null) {
            $cols = $this->resultado->numColumns();
            for ($i = 0; $i < $cols; $i++) {
                $array[] = $this->resultado->columnName($i);
            }
        }
        return $array;
    }

    /**
     * Retorna el resultado de una consulta (Dependiendo mucho del tipo de resultado enviado Ver línea 70)
     * @param string $resultadoTipo
     * @return boolean|array
     */
    public function get_siguiente($resultadoTipo = VNZ_VALOR_PREDEFINIDO) {
        if ($resultadoTipo == VNZ_VALOR_PREDEFINIDO) {
            $resultadoTipo = $this->prefResTipo;
        }
        $res = array();
        if ($this->resultado != null) {
            switch ($resultadoTipo) {
                case VNZ_RES_ASSOC:
                    $res = $this->resultado->fetchArray(SQLITE3_ASSOC);
                    break;
                case VNZ_RES_NUM:
                    $res = $this->resultado->fetchArray(SQLITE3_NUM);
                    break;
                case VNZ_RES_AMBAS:
                    $res = $this->resultado->fetchArray(SQLITE3_BOTH);
                    break;

                default:
                    $this->error = "Tipo de resultado incorrecto!";
                    return false;
            }
        }
        return $res;
    }

    /**
     * Retorna un resultado completo de una consulta
     * @param string $resultadoTipo
     * @return boolean|array
     */
    public function get_todo($resultadoTipo = VNZ_VALOR_PREDEFINIDO) {
        if ($resultadoTipo == VNZ_VALOR_PREDEFINIDO) {
            $resultadoTipo = $this->prefResTipo;
        }
        $row = array();
        if ($this->resultado != null) {
            switch ($resultadoTipo) {
                case VNZ_RES_ASSOC:
                    while ($res = $this->resultado->fetchArray(SQLITE3_ASSOC)) {
                        array_push($row, $res);
                    }
                    break;
                case VNZ_RES_NUM:
                    while ($res = $this->resultado->fetchArray(SQLITE3_NUM)) {
                        array_push($row, $res);
                    }
                    break;
                case VNZ_RES_AMBAS:
                    while ($res = $this->resultado->fetchArray(SQLITE3_BOTH)) {
                        array_push($row, $res);
                    }
                    break;
                default:
                    $this->error = "Tipo de resultado incorrecto!";
                    return false;
            }
        }
        return $row;
    }

    /**
     * Retorna el número de rows en una consulta
     * @return int|boolean
     */
    public function numero_rows() {
        if ($this->resultado != null) {
            $num_rows = 0;
            while ($row = @$this->resultado->fetchArray(SQLITE3_ASSOC)) {
                $num_rows++;
            }
            return $num_rows;
        } else {
            $this->error = "numero_rows resultado FALSE";
            return false;
        }
    }

    /**
     * Liberar el resultado de una consulta
     * @return boolean
     */
    public function liberar() {
        if ($this->resultado != null) {
            $this->resultado->finalize();
            return TRUE;
        } else {
            $this->error = "liberar FALSE";
            return FALSE;
        }
    }

    /**
     * Realiza una consulta a la base de datos retorna un objeto
     * @return boolean|object
     */
    public function fobject() {
        if ($this->resultado != null) {
            $res = $this->resultado->fetchArray(SQLITE3_ASSOC);
            if ($res != false) {
                return (object) $res;
            } else {
                return false;
            }
        } else {
            $this->error = "fobject FALSE";
            return false;
        }
    }

    /**
     * Realiza una consulta a la base de datos retorna un único objeto
     * @param string $query
     * @return boolean|object
     */
    public function get_uno($query) {
        if ($this->db != NULL) {
            //$rs = $this->db->querySingle($query);
            $rs = $this->db->query($query);
            if ($rs != NULL) {
                $res = $rs->fetchArray(SQLITE3_ASSOC);
                if ($res != false) {
                    return (object) $res;
                } else {
                    return false;
                }
            } else {
                $this->error = "get_uno query error";
                return FALSE;
            }
        }
    }

    /**
     * Retorna el número de rows afectados por una consulta
     * @return boolean|integer
     */
    public function rows_afectados() {
        if ($this->db != null) {
            if ($this->resultado != false) {
                return $this->db->changes();
            } else {
                $this->error = "rows_afectados this resultado FALSE";
                return false;
            }
        } else {
            $this->error = "rows_afectados This DB FALSE";
            return false;
        }
    }

    /**
     * Realiza la Desconexión
     * @return boolean
     */
    public function desconectar() {
        parent::desconectar();
        if ($this->db != null) {
            $this->db->close();
            $this->_AntesDesconectar();
            return true;
        } else {
            $this->error = "No Conectado a ninguna Base de Datos!";
            return false;
        }
    }

}

?>