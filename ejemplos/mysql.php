<?php

require_once('../vnzsql/base/class.basevnz.php');
require('../vnzsql/class.mysqldb.php');

//$cbd = new vnzsql_mysql();
$cbd = vnzsql_mysql::singleton();
// host database usuario password puerto
$cbd->conectar("localhost", "vnzsql_pruebas", "root", "", 3306);
$cbd->set_charset("utf8");

//$cbd->set_tiporespuesta(VNZ_RES_ASSOC);

//var_dump($cbd->get_variable("SELECT id,nombre FROM usuarios WHERE direccion = 'venezuela'",1));

/*
$cbd->query("select * from usuarios");
while($row = $cbd->get_siguiente()){
    echo $row["nombre"]."<br/>";
}
SALIDA:
Misael Black
Pedro Fernandez
Carlos Jojoto

$cbd->query("select * from usuarios");
print_r($cbd->get_tablas());
SALIDA:
Array
(
    [0] => usuarios
)

echo $cbd->escape(" misael' ");
SALIDA:
misael\' 

$cbd->query("select * from usuarios");
print_r($cbd->get_nombrecolumnas());
SALIDA:
Array
(
    [0] => id
    [1] => nombre
    [2] => email
    [3] => direccion
)

$cbd->query("select * from usuarios");
print_r($cbd->get_columnas());
SALIDA:
Array
(
    [0] => Array
        (
            [id] => 1
            [nombre] => Misael Black
            [email] => blacksecured@gmail.com
            [direccion] => venezuela
        )

    [1] => Array
        (
            [id] => 2
            [nombre] => Pedro Fernandez
            [email] => pedrito@correo.com
            [direccion] => panama
        )

    [2] => Array
        (
            [id] => 3
            [nombre] => Carlos Jojoto
            [email] => jojoto@email.com
            [direccion] => ecuador
        )

)

echo $cbd->get_identificador();
SALIDA: 
MYSQL

echo $cbd->get_dbtipo(); 
SALIDA:
mysql

echo $cbd->get_version();
SALIDA:
VNZSQL: 0.1

$rs = $cbd->query("INSERT INTO usuarios (nombre,email,direccion) VALUES ('manuel','manuel@jmail.com','chile')");
if($rs){
    echo "Insertado Con exito";
}else{
    echo "Error al insertar?";
    echo $cbd->error;
}

$valor = $cbd->get_uno("SELECT * FROM usuarios WHERE direccion = 'venezuela'");
echo $valor->nombre;
SALIDA:
Misael Black

$cbd->query("SELECT * FROM usuarios");
print_r($cbd->get_siguiente());
SALIDA:
Array
(
    [id] => 1
    [nombre] => Misael Black
    [email] => blacksecured@gmail.com
    [direccion] => venezuela
)

$cbd->query("SELECT * FROM usuarios");
$array = $cbd->get_siguiente();
$objeto = $cbd->array2obj($array);
echo $objeto->nombre;
SALIDA:
Misael Black

$cbd->query("SELECT * FROM usuarios");
print_r($cbd->get_todo());
SALIDA:
Array
(
    [0] => Array
        (
            [id] => 1
            [nombre] => Misael Black
            [email] => blacksecured@gmail.com
            [direccion] => venezuela
        )

    [1] => Array
        (
            [id] => 2
            [nombre] => Pedro Fernandez
            [email] => pedrito@correo.com
            [direccion] => panama
        )

    [2] => Array
        (
            [id] => 3
            [nombre] => Carlos Jojoto
            [email] => jojoto@email.com
            [direccion] => ecuador
        )

)

$cbd->query("SELECT * FROM usuarios WHERE email = 'blacksecured@gmail.com'");
$row = $cbd->get_siguiente();
echo "Usuario: ".$row["nombre"]."<br />";
echo "Direccion: ".$row["direccion"]."<br />";
SALIDA:
Usuario: Misael Black
Direccion: venezuela

$cbd->query("SELECT * FROM usuarios");
echo $cbd->rows_afectados();
SALIDA:
3

$cbd->query("SELECT * FROM usuarios where nombre = 'Misael Black' ");
if($cbd->numero_rows() > 0){
    echo "bien";
}else{
    echo "nada";
}

$cbd->query("SELECT * FROM usuarios where nombre = 'Misael Black' ");
echo $cbd->numero_rows();
SALIDA:
1

$cbd->query("SELECT * FROM usuarios");
echo $cbd->numero_rows();
SALIDA:
3
*/

//$cbd->liberar();

$cbd->desconectar();

unset($cbd);


?>