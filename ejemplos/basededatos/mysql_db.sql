--
-- MYSQL DATABASE DE EJEMPLO PARA VNZSQL
--

CREATE DATABASE IF NOT EXISTS `vnzsql_pruebas` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `vnzsql_pruebas`;

drop table if exists usuarios;

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `direccion` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

INSERT INTO `usuarios` (`id`, `nombre`, `email`, `direccion`) VALUES
(1, 'Misael Black', 'blacksecured@gmail.com', 'venezuela');

INSERT INTO `usuarios` (`id`, `nombre`, `email`, `direccion`) VALUES
(2, 'Pedro Fernandez', 'pedrito@correo.com', 'panama');

INSERT INTO `usuarios` (`id`, `nombre`, `email`, `direccion`) VALUES
(3, 'Carlos Jojoto', 'jojoto@email.com', 'ecuador');
