--
-- SQLITE3 DATABASE DE EJEMPLO PARA VNZSQL
--

drop table if exists usuarios;

create table if not exists usuarios(
    id integer primary key AUTOINCREMENT,
    nombre varchar(150) not null,
    email varchar(150) not null,
    direccion varchar(150) not null
);

INSERT INTO `usuarios` (`id`, `nombre`, `email`, `direccion`) VALUES
(1, 'Misael Black', 'blacksecured@gmail.com', 'venezuela');

INSERT INTO `usuarios` (`id`, `nombre`, `email`, `direccion`) VALUES
(2, 'Pedro Fernandez', 'pedrito@correo.com', 'panama');

INSERT INTO `usuarios` (`id`, `nombre`, `email`, `direccion`) VALUES
(3, 'Carlos Jojoto', 'jojoto@email.com', 'ecuador');
